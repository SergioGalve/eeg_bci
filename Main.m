%%% MAIN
% We will launch from here all the functions to read the data from the edf
% files and process it up to a certain point
% https://www.physionet.org/physiobank/database/eegmmidb/

%% Global Variables
c3=9;
cz=11;
c4=13;
w_opt = 2;
w_size= 160*[0.2 0.5];
subjects = 9;
tests = 9;

notch = 50;
band = [5 80];
freq = 160;

file = '/home/sergio/EEG_testing/Database_samples/S0';

eeg = {};

%% Reading the data
% we will store the data in time tables inside a cell for the same
% experiment, inside a cell with the other cell/experiments
[A,B,C,D] = butter(10,[5 78]/(freq*0.5));
band_f = designfilt('bandpassiir','FilterOrder',20, ...
    'HalfPowerFrequency1',5,'HalfPowerFrequency2',78, ...
    'SampleRate',freq);
sos = ss2sos(A,B,C,D);
Hd = dfilt.statespace(A,B,C,D)
% fvt = fvtool(sos,band_f,'Fs',1500);
% legend(fvt,'butter','designfilt')

notch_f = designfilt('bandstopiir','FilterOrder',2, ...
               'HalfPowerFrequency1',59,'HalfPowerFrequency2',61, ...
               'DesignMethod','butter','SampleRate',freq);
           
           
for i=1:subjects
    for j=1:tests
        name=strcat(file,int2str(0),int2str(i),'R',int2str(0),int2str(j),'.edf');
        [hdr,data]=edfread(name);
        data= data([c3 cz c4],200:end-200)';
        filtered = filter(Hd,data);
        filtered_2 = filtfilt(notch_f,filtered);
        eeg{j}{i} = filtered_2;
    end
end
% t = (0:length(data(:,1))-1)/freq;
% plot(t,data(:,1),t,filtered_2)
% ylabel('Voltage (uV)')
% xlabel('Time (s)')
% title('EEG for imaginary motion')
% legend('Unfiltered','Notch Filtered')
% grid

%% Splitting the data --> Feature extraction
% We want to group the data in small windss that we can relate to the
% phisiology of the mental responso inside the experiment
f1 = freq*(0:(w_size(1)/2))/w_size(1);
f2 = freq*(0:(w_size(2)/2))/w_size(2);
database_1 = {{{}}};
database_2 = {{{}}};
for j=1:tests
    for i=1:subjects
        n_1 = 1;
        n_2 = 1;
        block = eeg{j}{i};
        for n=1:floor(length(block(:,1))/w_size(1))
            piece=block(((n-1)*w_size(1)+1):((n)*w_size(1)+1),:);
            Y=fft(piece);
            P2 = abs(Y/w_size(1));
            P1 = P2(1:w_size(1)/2+1,:);
            P1(2:end-1,:) = 2*P1(2:end-1,:);
            database_1{j}{i}{n_1}=P1;
            n_1 = n_1 + 1;
        end

        for n=1:floor(length(block(:,1))/w_size(2))
            piece=block(((n-1)*w_size(2)+1):((n)*w_size(2)+1),:);
            Y=fft(piece);
            P2 = abs(Y/w_size(2));
            P1 = P2(1:w_size(2)/2+1,:);
            P1(2:end-1,:) = 2*P1(2:end-1,:);
            database_2{j}{i}{n_2}=P1;
            n_2 = n_2 + 1;
        end
        
    end
end

% database structure: [Test Subject windss] 
save('C:\users\sergi\Desktop\TFM\EEG_testing\Databases_9_9.mat','database_1','database_2')
% plot(f2,P1) 
% title('Single-Sided Amplitude Spectrum of X(t)')
% xlabel('f (Hz)')
% ylabel('|P1(f)|')
%% Visualization
vis = 1;
data = database_1;
%userss in rows -->
users = [1 2 3 4];
%windss in columns ^|
winds = [3 5 7 9];
experiments = [1 2 3 4];
if vis==0
    for i=1:length(experiments)
        f = figure
%         p = uipanel('Parent',f,'BorderType','none'); 
%         name = strcat('Experiment #',int2str(experiments(i)));
%         p.Title = name; 
%         p.TitlePosition = 'centertop'; 
%         p.FontSize = 12;
%         p.FontWeight = 'bold';
%         xlabel('f (Hz)')
%         ylabel('|P1(f)|')
        %1st users
        ax1 = subplot(4,4,1);
        plot(f1,data{experiments(i)}{users(1)}{winds(1)}) 
        title(strcat('Subject #',int2str(users(1)),' | winds #',int2str(winds(1))));
        ax2 = subplot(4,4,2);
        plot(f1,data{experiments(i)}{users(1)}{winds(2)}) 
        title(strcat('Subject #',int2str(users(1)),' | winds #',int2str(winds(2))));
        ax3 = subplot(4,4,3);
        plot(f1,data{experiments(i)}{users(1)}{winds(3)}) 
        title(strcat('Subject #',int2str(users(1)),' | winds #',int2str(winds(3))));
        ax4 = subplot(4,4,4);
        plot(f1,data{experiments(i)}{users(1)}{winds(4)}) 
        title(strcat('Subject #',int2str(users(1)),' | winds #',int2str(winds(4))));
        %2nd users
        ax5 = subplot(4,4,5);
        plot(f1,data{experiments(i)}{users(2)}{winds(1)}) 
        title(strcat('Subject #',int2str(users(2)),' | winds #',int2str(winds(1))));
        ax6 = subplot(4,4,6);
        plot(f1,data{experiments(i)}{users(2)}{winds(2)}) 
        title(strcat('Subject #',int2str(users(2)),' | winds #',int2str(winds(2))));
        ax7 = subplot(4,4,7);
        plot(f1,data{experiments(i)}{users(2)}{winds(3)}) 
        title(strcat('Subject #',int2str(users(2)),' | winds #',int2str(winds(3))));
        ax8 = subplot(4,4,8);
        plot(f1,data{experiments(i)}{users(2)}{winds(4)}) 
        title(strcat('Subject #',int2str(users(2)),' | winds #',int2str(winds(4))));
        %3rd users
        ax9 = subplot(4,4,9);
        plot(f1,data{experiments(i)}{users(3)}{winds(1)}) 
        title(strcat('Subject #',int2str(users(3)),' | winds #',int2str(winds(1))));
        ax10 = subplot(4,4,10);
        plot(f1,data{experiments(i)}{users(3)}{winds(2)}) 
        title(strcat('Subject #',int2str(users(3)),' | winds #',int2str(winds(2))));
        ax11 = subplot(4,4,11);
        plot(f1,data{experiments(i)}{users(3)}{winds(3)}) 
        title(strcat('Subject #',int2str(users(3)),' | winds #',int2str(winds(3))));
        ax12 = subplot(4,4,12);
        plot(f1,data{experiments(i)}{users(3)}{winds(4)}) 
        title(strcat('Subject #',int2str(users(3)),' | winds #',int2str(winds(4))));

        %4th users
        ax13 = subplot(4,4,13);
        plot(f1,data{experiments(i)}{users(4)}{winds(1)}) 
        title(strcat('Subject #',int2str(users(4)),' | winds #',int2str(winds(1))));
        ax14 = subplot(4,4,14);
        plot(f1,data{experiments(i)}{users(4)}{winds(2)}) 
        title(strcat('Subject #',int2str(users(4)),' | winds #',int2str(winds(2))));
        ax15 = subplot(4,4,15);
        plot(f1,data{experiments(i)}{users(4)}{winds(3)}) 
        title(strcat('Subject #',int2str(users(4)),' | winds #',int2str(winds(3))));
        ax16 = subplot(4,4,16);
        plot(f1,data{experiments(i)}{users(4)}{winds(4)}) 
        title(strcat('Subject #',int2str(users(4)),' | winds #',int2str(winds(4))));
    end
end

%% Analysing the data 
% Different classification methods and the obtained performance
% We must compare the classifier just for one subject vs for all of them
% I: Fourier energy levels
% Best Band Selection 
data = database_1;
rest=zeros(size(data{5}{5}{5}));
activity=zeros(size(data{5}{5}{5}));
n_r = 0;
n_a = 0;

% we should make the average of the percentual 'strength' of each value to
% be considered and compare to the average results

%absolute value check
for j=1:tests
    for i=1:subjects
        s = size(data{j}{i});
        for n = 1:s(2)
           if j<3
               n_r=n_r+1;
               rest=rest+data{j}{i}{n};
           else
               n_a=n_a+1;
               activity=activity+data{j}{i}{n};
           end
            
        end
    end
end
ac = [sum(activity(:,1)) sum(activity(:,2)) sum(activity(:,3))];
activity = [activity(:,1)/ac(1) activity(:,2)/ac(2) activity(:,2)/ac(2)];
re = [sum(rest(:,1)) sum(rest(:,2)) sum(rest(:,3))];
rest = [rest(:,1)/re(1) rest(:,2)/re(2) rest(:,2)/re(2)];
diff_1 = abs(activity-rest);

%%%%%%%%%%%%% (re-set up)
data = database_1;
rest=zeros(size(data{5}{5}{5}));
activity=zeros(size(data{5}{5}{5}));
n_r = 0;
n_a = 0;
%relative value check
for j=1:tests
    for i=1:subjects
        s = size(data{j}{i});
        for n = 1:s(2)
           if n==1
               pie = data{j}{i}{n};
               rel = [max(pie(:,1)) max(pie(:,2)) max(pie(:,3))];
           end
           if j<3
               n_r=n_r+1;
               rest=rest+[data{j}{i}{n}(:,1)/rel(1) data{j}{i}{n}(:,2)/rel(2) data{j}{i}{n}(:,3)/rel(3)];
           else
               n_a=n_a+1;
               activity=activity+[data{j}{i}{n}(:,1)/rel(1) data{j}{i}{n}(:,2)/rel(2) data{j}{i}{n}(:,3)/rel(3)];
           end
            
        end
    end
end
rest = rest/n_r
activity = activity/n_a
diff_2 = abs(activity-rest);
features = 5;
[val,c3_f] = maxk(diff_2(:,1),features);
[val,cz_f] = maxk(diff_2(:,2),features);
[val,c4_f] = maxk(diff_2(:,3),features);

% Distance Multyplane based classifier
% we pass all the values to a format suitable for the classification
    %Labels as well
X_9_9 = [];
Y_9_9 = [];
num =0;
for j=1:tests
    for i=1:subjects
        s = size(data{j}{i});
        for n = 1:s(2)
           if n==1
               pie = data{j}{i}{n};
               rel = [max(pie(:,1)) max(pie(:,2)) max(pie(:,3))];
           end 
           num = num + 1;
           res = [data{j}{i}{n}(:,1)/rel(1) data{j}{i}{n}(:,2)/rel(2) data{j}{i}{n}(:,3)/rel(3)];
           X_9_9(:,num) = [res(c3_f,1);res(cz_f,2);res(c4_f,3)];
           if j<3
               Y_9_9(num) = 0; %tag 0 means rest (eyes closed or open indistintly)
           elseif j==3 || j==7
               Y_9_9(num) = 1; %tag 1 means task 1 (right or left indistintly)
           elseif j==4 || j==8
               Y_9_9(num) = 1; %tag 2 means task 2 (right or left indistintly)
           elseif j==5 || j==9
               Y_9_9(num) = 1; %tag 3 means task 3 (right or left indistintly)
           elseif j==6
               Y_9_9(num) = 1;
           end
            
        end
    end
end

database_class = [X_9_9; Y_9_9];
save('C:\users\sergi\Desktop\TFM\EEG_testing\Databases_9_9.mat','X_9_9','Y_9_9')

% II: Recurrent neuralnet





